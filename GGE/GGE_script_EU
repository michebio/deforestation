/*
 Script to extract data of deforestation status in europe.
 The script should be run with Google Earth Engine: https://code.earthengine.google.com/
 I use the dataset published by Hansen et al. Science (2013) to estimate
 forest cover and loss from 2000 to 2019.
 See http://earthenginepartners.appspot.com/science-2013-global-forest
 for dataset details.

 International country boundaries are extracted from http://geonode.state.gov/
*/


// Select country boundaries
/*var countries = ee.FeatureCollection('USDOS/LSIB_SIMPLE/2017');
var austria = countries.filter(ee.Filter.eq('country_na', 'Austria'));
var belgium = countries.filter(ee.Filter.eq('country_na', 'Belgium'));
var bulgaria = countries.filter(ee.Filter.eq('country_na', 'Bulgaria'));
var croatia = countries.filter(ee.Filter.eq('country_na', 'Croatia'));
var italy = countries.filter(ee.Filter.eq('country_na', 'Italy'));
var Denmark = countries.filter(ee.Filter.eq('country_na', 'Denmark'));
var Estonia = countries.filter(ee.Filter.eq('country_na', 'Estonia'));
var Finland = countries.filter(ee.Filter.eq('country_na', 'Finland'));
var France = countries.filter(ee.Filter.eq('country_na', 'France'));
var Germany = countries.filter(ee.Filter.eq('country_na', 'Germany'));
var Greece = countries.filter(ee.Filter.eq('country_na', 'Greece'));
var Hungary = countries.filter(ee.Filter.eq('country_na', 'Hungary'));
var Ireland = countries.filter(ee.Filter.eq('country_na', 'Ireland'));
var Latvia = countries.filter(ee.Filter.eq('country_na', 'Latvia'));
var Lithuania = countries.filter(ee.Filter.eq('country_na', 'Lithuania'));
var Luxembourg = countries.filter(ee.Filter.eq('country_na', 'Luxembourg'));
var Malta = countries.filter(ee.Filter.eq('country_na', 'Malta'));
var Netherlands = countries.filter(ee.Filter.eq('country_na', 'Netherlands'));
var Poland = countries.filter(ee.Filter.eq('country_na', 'Poland'));
var Portugal = countries.filter(ee.Filter.eq('country_na', 'Portugal'));
var Romania = countries.filter(ee.Filter.eq('country_na', 'Romania'));
var Slovakia = countries.filter(ee.Filter.eq('country_na', 'Slovakia'));
var Slovenia = countries.filter(ee.Filter.eq('country_na', 'Slovenia'));
var Spain = countries.filter(ee.Filter.eq('country_na', 'Spain'));
var Sweden = countries.filter(ee.Filter.eq('country_na', 'Sweden'));
var cz = countries.filter(ee.Filter.eq('country_na', 'Czechia'));
var Cyprus = countries.filter(ee.Filter.eq('country_na', 'Cyprus'));
var Switzerland = countries.filter(ee.Filter.eq('country_na', 'Switzerland'));
print('italy', italy);
var europe = austria.merge(italy).merge(belgium).merge(bulgaria).merge(croatia)
.merge(cz).merge(Cyprus).merge(Denmark).merge(Estonia)
.merge(Finland).merge(France).merge(Germany).merge(Greece)
.merge(Hungary).merge(Ireland).merge(Latvia).merge(Lithuania)
.merge(Luxembourg).merge(Malta).merge(Netherlands).merge(Poland)
.merge(Portugal).merge(Romania).merge(Slovakia).merge(Slovenia).merge(Spain).merge(Sweden);
*/
var europe = ee.Geometry.Rectangle([-13, 30, 30, 55]);

// load hansen dataset
var gfc2019 = ee.Image("UMD/hansen/global_forest_change_2019_v1_7");
print('gfc2019', gfc2019);




/*
// Sum the values of forest loss pixels in europe.
var stats2019 = area2019.reduceRegion({
  reducer: ee.Reducer.sum(),
  geometry: europe,
  scale: 2000,
  maxPixels: 1e9
});
print('pixels representing loss: ', stats2019.get('loss'), 'square meters');
*/

// calculate loss by year.
// See tutorial https://developers.google.com/earth-engine/tutorials/tutorial_forest_03a
var lossByYear = gfc2019.select(['loss']).multiply(ee.Image.pixelArea())
  .addBands(gfc2019.select(['lossyear'])).reduceRegion({
  reducer: ee.Reducer.sum().group({
    groupField: 1
    }),
  geometry: europe,
  scale: 2000,
  maxPixels: 1e9
});
print('lossByYear', lossByYear);

var statsFormattedLoss = ee.List(lossByYear.get('groups'))
  .map(function(el) {
    var d = ee.Dictionary(el);
    return [ee.Number(d.get('group')).format("20%02d"), ee.Number(d.get('sum')).multiply(0.000001).round()];
  });
var statsDictionaryLoss = ee.Dictionary(statsFormattedLoss.flatten());
print('statsDictionary', statsDictionaryLoss);

// make an histogram of the data
var chart_en = ui.Chart.array.values({
  array:  statsDictionaryLoss.values(),
  axis: 0,
  xLabels: statsDictionaryLoss.keys()
}).setChartType('ColumnChart')
  .setOptions({
    title: '', //Yearly forest loss in the plotted area from year 2000 to 2019',
    hAxis: {title: 'Year', format: '####'},
    vAxis: {title: 'Area (square km)'},
    legend: { position: "none" },
    lineWidth: 1,
    pointSize: 3,
    colors: ['DC143C']// ['0000FF'] use for blue
  });
print(chart_en);


// same plot with Italian captions
var chart_it = ui.Chart.array.values({
  array:  statsDictionaryLoss.values(),
  axis: 0,
  xLabels: statsDictionaryLoss.keys()
}).setChartType('ColumnChart')
  .setOptions({
    title: "",
    hAxis: {title: 'Anno', format: '####'},
    vAxis: {title: 'Area (km quadrati)'},
    legend: { position: "none" },
    lineWidth: 1,
    pointSize: 3,
    colors: ['DC143C']// ['0000FF'] use for blue
  });
print(chart_it);

// check loss year band
//var lossyear =  gfc2019.select(['lossyear']);
//print('lossyear', gfc2019.select(['lossyear']));


/// create a MAP to visualize within the engine

/*
Map.addLayer(gfc2019, {
  bands: ['loss', 'treecover2000', 'gain'],
  max: [1, 255, 1]
}, 'forest cover, loss, gain');
*/

var treecover2000 = gfc2019.select(['treecover2000']).updateMask(gfc2019.select(['treecover2000']))
  .clip(europe);
var loss = gfc2019.select(['loss']).updateMask(gfc2019.select(['loss'])).clip(europe);
var gain = gfc2019.select(['gain']).updateMask(gfc2019.select(['gain'])).clip(europe);

// Add the tree cover layer in green.
Map.addLayer(treecover2000,
    {palette: ['000000', '00FF00'], max: 100}, 'Forest Cover 2000');

// Add the loss layer in red.
Map.addLayer(loss,
            {palette: ['DC143C']}, 'Loss');

// Add the gain layer in blue.
Map.addLayer(gain,
            {palette: ['0000FF']}, 'Gain');

// Save and export the map as a tiff file
var visualization = gfc2019.clip(europe).visualize({
  bands: ['treecover2000', 'loss', 'gain'],
  max: [100, 1, 1]
});
Export.image.toDrive({
  image: visualization,
  description: 'tree_cover_2000_hansen',
  skipEmptyTiles: true,
  scale: 2000,
  //dimensions: 3,
  //maxPixels: 10,
  region: europe
});

// alternative version of the same map allowing a higher max value (fewer contrasts)
var visualization255 = gfc2019.clip(europe).visualize({
  bands: ['treecover2000', 'loss', 'gain'],
  max: [255, 1, 1]
});

Export.image.toDrive({
  image: visualization255,
  description: 'tree_cover_2000_hansen_255max',
  skipEmptyTiles: true,
  scale: 2000,
  //dimensions: 3,
  //maxPixels: 10,
  region: europe
});



// load earlier versions of the data to check for deforestation statuses
// in previous years
var gfc2013 = ee.Image("UMD/hansen/global_forest_change_2014");
var gfc2015 = ee.Image("UMD/hansen/global_forest_change_2015_v1_3");
var gfc2016 = ee.Image("UMD/hansen/global_forest_change_2016_v1_4");
var gfc2017 = ee.Image("UMD/hansen/global_forest_change_2017_v1_5");
var gfc2018 = ee.Image("UMD/hansen/global_forest_change_2018_v1_6");


// Export losses and gains for different years
var vis2013 = gfc2013.clip(europe).visualize({
  bands: ['loss', 'gain'],
  max: [1, 1]
});
Export.image.toDrive({
  image: vis2013,
  description: 'vis2013',
  skipEmptyTiles: true,
  scale: 2000,
  //dimensions: 3,
  //maxPixels: 10,
  region: europe
});
var vis2015 = gfc2015.clip(europe).visualize({
  bands: ['loss', 'gain'],
  max: [1, 1]
});
Export.image.toDrive({
  image: vis2015,
  description: 'vis2015',
  skipEmptyTiles: true,
  scale: 2000,
  //dimensions: 3,
  //maxPixels: 10,
  region: europe
});
var vis2016 = gfc2016.clip(europe).visualize({
  bands: ['loss', 'gain'],
  max: [1, 1]
});
Export.image.toDrive({
  image: vis2016,
  description: 'vis2016',
  skipEmptyTiles: true,
  scale: 2000,
  //dimensions: 3,
  //maxPixels: 10,
  region: europe
});
var vis2017 = gfc2017.clip(europe).visualize({
  bands: ['loss', 'gain'],
  max: [1, 1]
});
Export.image.toDrive({
  image: vis2017,
  description: 'vis2017',
  skipEmptyTiles: true,
  scale: 2000,
  //dimensions: 3,
  //maxPixels: 10,
  region: europe
});
var vis2018 = gfc2018.clip(europe).visualize({
  bands: ['loss', 'gain'],
  max: [1, 1]
});
Export.image.toDrive({
  image: vis2018,
  description: 'vis2018',
  skipEmptyTiles: true,
  scale: 2000,
  //dimensions: 3,
  //maxPixels: 10,
  region: europe
});
var vis2019 = gfc2019.clip(europe).visualize({
  bands: ['loss', 'gain'],
  max: [1, 1]
});
Export.image.toDrive({
  image: vis2019,
  description: 'vis2019',
  skipEmptyTiles: true,
  scale: 2000,
  //dimensions: 3,
  //maxPixels: 10,
  region: europe
});


print('the end');

/* After sussesfully running the script above, tab on Tasks.
  Run each single task, and save on your drive before.
  Then you can download each single tiff file onto your local PC and save the files
  into a convenient location for further analyses/manipulation.
*/

