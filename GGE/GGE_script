/*
 Script to extract data of deforestation status in Italy.
 The script should be run with Google Earth Engine: https://code.earthengine.google.com/
 I use the dataset published by Hansen et al. Science (2013) to estimate
 forest cover and loss from 2000 to 2019.
 See http://earthenginepartners.appspot.com/science-2013-global-forest
 for dataset details.

 International country boundaries are extracted from http://geonode.state.gov/
*/


// Select country boundaries
var countries = ee.FeatureCollection('USDOS/LSIB_SIMPLE/2017');
var italy = countries.filter(ee.Filter.eq('country_na', 'Italy'));


// load hansen dataset
var gfc2019 = ee.Image("UMD/hansen/global_forest_change_2019_v1_7");
print('gfc2019', gfc2019);

var test = gfc2019.select(['lossyear']);
var loss2019 = gfc2019.select(['loss']);
var area2019 = loss2019.multiply(ee.Image.pixelArea());

// Sum the values of forest loss pixels in italy.
var stats2019 = area2019.reduceRegion({
  reducer: ee.Reducer.sum(),
  geometry: italy,
  scale: 30,
  maxPixels: 1e9
});
print('pixels representing loss: ', stats2019.get('loss'), 'square meters');


// calculate loss by year.
// See tutorial https://developers.google.com/earth-engine/tutorials/tutorial_forest_03a
var lossByYear = area2019.addBands(gfc2019.select(['lossyear'])).reduceRegion({
  reducer: ee.Reducer.sum().group({
    groupField: 1
    }),
  geometry: italy,
  scale: 30,
  maxPixels: 1e9
});
var lossByYear2019 =  ee.List(lossByYear.get('groups'));

print('lossByYear2019', lossByYear2019);
print('lossByYear', lossByYear);

var statsFormattedLoss = ee.List(lossByYear.get('groups'))
  .map(function(el) {
    var d = ee.Dictionary(el);
    return [ee.Number(d.get('group')).format("20%02d"), ee.Number(d.get('sum')).round()];
  });
var statsDictionaryLoss = ee.Dictionary(statsFormattedLoss.flatten());
print('statsDictionary', statsDictionaryLoss);

// make an histogram of the data
var chart_en = ui.Chart.array.values({
  array:  statsDictionaryLoss.values(),
  axis: 0,
  xLabels: statsDictionaryLoss.keys()
}).setChartType('ColumnChart')
  .setOptions({
    title: 'Yearly forest loss in Italy from year 2000',
    hAxis: {title: 'Year', format: '####'},
    vAxis: {title: 'Area (square meters)'},
    legend: { position: "none" },
    lineWidth: 1,
    pointSize: 3,
    colors: ['DC143C']// ['0000FF'] use for blue
  });
print(chart_en);

// same plot with Italian captions
var chart_it = ui.Chart.array.values({
  array:  statsDictionaryLoss.values(),
  axis: 0,
  xLabels: statsDictionaryLoss.keys()
}).setChartType('ColumnChart')
  .setOptions({
    title: 'Perdita annuale di foresta in Italia dal 2000',
    hAxis: {title: 'Anno', format: '####'},
    vAxis: {title: 'Area (metri quadrati)'},
    legend: { position: "none" },
    lineWidth: 1,
    pointSize: 3,
    colors: ['DC143C']// ['0000FF'] use for blue
  });
print(chart_it);

// check loss year band
var lossyear =  gfc2019.select(['lossyear']);
print('lossyear', gfc2019.select(['lossyear']));


/// create a MAP to visualize within the engine

/*
Map.addLayer(gfc2019, {
  bands: ['loss', 'treecover2000', 'gain'],
  max: [1, 255, 1]
}, 'forest cover, loss, gain');
*/

var treecover2000 = gfc2019.select(['treecover2000']).updateMask(gfc2019.select(['treecover2000']))
  .clip(italy);
var loss = gfc2019.select(['loss']).updateMask(gfc2019.select(['loss'])).clip(italy);
var gain = gfc2019.select(['gain']).updateMask(gfc2019.select(['gain'])).clip(italy);

// Add the tree cover layer in green.
Map.addLayer(treecover2000,
    {palette: ['000000', '00FF00'], max: 100}, 'Forest Cover 2000');

// Add the loss layer in red.
Map.addLayer(loss,
            {palette: ['DC143C']}, 'Loss');

// Add the gain layer in blue.
Map.addLayer(gain,
            {palette: ['0000FF']}, 'Gain');

// Save and export the map as a tiff file
var visualization = gfc2019.clip(italy).visualize({
  bands: ['treecover2000', 'loss', 'gain'],
  max: [100, 1, 1]
});
Export.image.toDrive({
  image: visualization,
  description: 'tree_cover_2000_hansen',
  skipEmptyTiles: true,
  scale: 300,
  //dimensions: 3,
  //maxPixels: 10,
  region: italy
});

// alternative version of the same map allowing a higher max value (fewer contrasts)
var visualization255 = gfc2019.clip(italy).visualize({
  bands: ['treecover2000', 'loss', 'gain'],
  max: [255, 1, 1]
});

Export.image.toDrive({
  image: visualization255,
  description: 'tree_cover_2000_hansen_255max',
  skipEmptyTiles: true,
  scale: 300,
  //dimensions: 3,
  //maxPixels: 10,
  region: italy
});



// load earlier versions of the data to check for deforestation statuses
// in previous years
var gfc2013 = ee.Image("UMD/hansen/global_forest_change_2014");
var gfc2015 = ee.Image("UMD/hansen/global_forest_change_2015_v1_3");
var gfc2016 = ee.Image("UMD/hansen/global_forest_change_2016_v1_4");
var gfc2017 = ee.Image("UMD/hansen/global_forest_change_2017_v1_5");
var gfc2018 = ee.Image("UMD/hansen/global_forest_change_2018_v1_6");


// Export losses and gains for different years
var vis2013 = gfc2013.clip(italy).visualize({
  bands: ['loss', 'gain'],
  max: [1, 1]
});
Export.image.toDrive({
  image: vis2013,
  description: 'vis2013',
  skipEmptyTiles: true,
  scale: 300,
  //dimensions: 3,
  //maxPixels: 10,
  region: italy
});
var vis2015 = gfc2015.clip(italy).visualize({
  bands: ['loss', 'gain'],
  max: [1, 1]
});
Export.image.toDrive({
  image: vis2015,
  description: 'vis2015',
  skipEmptyTiles: true,
  scale: 300,
  //dimensions: 3,
  //maxPixels: 10,
  region: italy
});
var vis2016 = gfc2016.clip(italy).visualize({
  bands: ['loss', 'gain'],
  max: [1, 1]
});
Export.image.toDrive({
  image: vis2016,
  description: 'vis2016',
  skipEmptyTiles: true,
  scale: 300,
  //dimensions: 3,
  //maxPixels: 10,
  region: italy
});
var vis2017 = gfc2017.clip(italy).visualize({
  bands: ['loss', 'gain'],
  max: [1, 1]
});
Export.image.toDrive({
  image: vis2017,
  description: 'vis2017',
  skipEmptyTiles: true,
  scale: 300,
  //dimensions: 3,
  //maxPixels: 10,
  region: italy
});
var vis2018 = gfc2018.clip(italy).visualize({
  bands: ['loss', 'gain'],
  max: [1, 1]
});
Export.image.toDrive({
  image: vis2018,
  description: 'vis2018',
  skipEmptyTiles: true,
  scale: 300,
  //dimensions: 3,
  //maxPixels: 10,
  region: italy
});
var vis2019 = gfc2019.clip(italy).visualize({
  bands: ['loss', 'gain'],
  max: [1, 1]
});
Export.image.toDrive({
  image: vis2019,
  description: 'vis2019',
  skipEmptyTiles: true,
  scale: 300,
  //dimensions: 3,
  //maxPixels: 10,
  region: italy
});


print('the end');

/* After sussesfully running the script above, tab on Tasks.
  Run each single task, and save on your drive before.
  Then you can download each single tiff file onto your local PC and save the files
  into a convenient location for further analyses/manipulation.
*/
