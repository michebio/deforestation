# Script to read the png images created by the script plots_eu.R
# and convert them into a single GIF.
# The images can be produced either in Italian or English.
# Specify the language of preference manually.

library(animation)
library(viridis) # nice color palette
library(ggplot2) # plotting
library(ggmap) # ggplot functionality for maps
library(dplyr) # use for fixing up data
library(readr) # reading in data/csv
library(RColorBrewer) # for color palettes
library(purrr) # for mapping over a function
library(magick) # this is call to animate/read pngs
library(png)

rm(list=ls())

# Pick language
# supported either en=English or ita=Italian
#lan <- "ita"
lan <- "en"
sec = 200
if(lan =="ita"){
  path_png <- "./plots/eu/ita"
  gif_name <- "/a_giff_deforestazione.gif" 
} else if(lan == "en"){
  path_png <- "./plots/eu/en"
  gif_name <- "/a_giff_deforestation.gif" 
}
# 23 images
list.files(path = path_png,  full.names = T, pattern = ".png") %>% 
map(image_read) %>% # reads each path file
image_join() %>% # joins image
image_animate(loop = 3, delay = sec) %>% # animates, can opt for number of loops
image_write(paste0(path_png, gif_name)) # write to file


