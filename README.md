# Code to plot forest cover change in Europe and create a short GIF

## The data

Data about forest cover from 2000 to 2019 were downloaded from [Google Earth Engine](http://earthenginepartners.appspot.com/science-2013-global-forest)

The dataset "Global forest change" is open-access. 
It was published by 
Hansen et al. "High-resolution global maps of 21st-century forest cover change." 
Science 342.6160 (2013): 850-853.
DOI: 10.1126/science.1244693 

Scripts used to download the data from Google Earth Engine (GEE) can be found in the folder GGE.
The scripts can be run directly on the online GEE code editor.
Data can be downloaded and saved on a machine as tiff.

## R scripts

The tiff files can be converted into png images with R.
R scripts ca be found within the plots folder.
The scripts "plots_eu.R" provides R code to produce the plots.
The script "makeGif.R" provides code to convert the plots into a GIF.



